#!/bin/sh
export PYTHONPATH=/home/ppindia/libs/jinja2/lib/python2.5/site-packages/
alias be=/home/ppindia/code/be/be
export REPOPATH=/home/ppindia/code/pirateparty-in

cd $REPOPATH

# Update repo first
git pull

# Generate html
be html

# Copy html files
rsync -av  html_export/ ~/pirateparty.org.in/todo
