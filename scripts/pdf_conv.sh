#!/bin/sh
usage()
{
	echo "pdf_conv <file> <outputfile>"
	exit 0;
}

if test -z $1
then
	usage
fi



HTML_FILE=$1
FILENAME=$(basename $HTML_FILE).pdf
if ! test -z $2
then
	FILENAME=$2;
fi

html2ps $HTML_FILE > temp.ps
ps2pdf temp.ps $FILENAME
rm temp.ps
