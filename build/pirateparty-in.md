--- fill

### Why Pirate Party? ###

* No one asked us how we think this world should be
* So we are remaking this world the way we think it should be
* Based on equality, democracy and transparency 

Version 1.0 Draft 4 ([Tue Jan 22 09:39:52 PST 2013](http://lists.pirateparty.org.in/pipermail/discuss-pirateparty.org.in/2013-January/003416.html))

        To navigate: Click on arrow buttons on either side. You can also use keyboard (use arrow keys, space, enter or backspace similar to a presentation/slide show). 

Download [the PDF version](http://pirateparty.org.in/constitution.pdf) or [source for this presentation/slide show](https://gitorious.org/pirateparty-in/pirateparty-in)

---

### Fix what is broken ###

* Peer-to-Peer vs hierarchical society
* Association of peers vs leaders and cadres
* Direct democracy vs Plutocracy

---

### Constitution ###

* Article 1: Basic Principles
* Article 2: Rights
* Article 3: Membership

---

### Article 1: Basic Principles ###

* Section 1: All humans are equal
* Section 2: We should cooperate with each other
* Section 3: Every one has a right to food, housing, education, healthcare and privacy
* Section 4: Knowledge should be free
* Section 5: We promote transparent government
* Section 6: We are opposed to the use of violence to achieve political aims. We will not, in any manner, promote or instigate or participate in violence. AND WE MEAN IT. However, we see violence as distinct from the issue it supports or opposes. The violence must be prosecuted through prevailing laws, and the issue must be evaluated through debate on its own merit.

--- 

### Article 2: Rights ###

* Section 1: Anyone supporting our basic principle can be an associate member
* Section 2: An associate member can apply for full membership after 6 months of working with the party
* Section 3: An associate member should demonstrate their commitment to the basic principles in their work -- through blogs, their social networks, by organzing events etc
* Section 4: They need support from at least two members

---

### Article 2: Rights ###

* Section 5: Working groups on specific issues
* Section 6: Working groups can propose policies and based on voting policy is decided
* Section 7: Policies are not binding to members and they have to promote only policies they agree with
* Section 8: Every member has a right to dissent and fork
* Section 9: Can campaign against party policy
* Section 10: Can fork and use Pirate Party name if they accept basic principles (add differentiating part)

---

### Article 3: Membership ###

* Section 1: Associate member

 * Sub-Section 1: Any one can apply to be an associate member if

   * they agree with our basic principles and constitution
   * they apply for associate membership

 * Sub-Section 2: Associate members may apply for full membership after 6
   months.

---

### Article 3: Membership ###

* Section 2: Initial Member

 * Subsection 1: Membership with 6 months validity only. Used for
   bootstrapping.

 * Subsection 2: Initial members have voting rights.

 * Subsection 3: Initial members may apply for full membership after 6 months.

 * Subsection 4: Intial members will be evaluated in the same way as
associate members (See Section 3 and 4 of Article 2)

--- fill

### Goals ###

* At the minimum we would like the ideas of peer-to-peer, direct democratic organization to spread in our society
* We would like to engage with more young and educated citizens and bring positive change with their involvement

--- fill

### Appendix 1: Glossary ###

* Advocate: when some one advocates, they indicate they support an
   application. ie, they have worked with the person or know that person.

* Violence: Violence is defined by the World Health Organization as the
intentional use of physical force or power, threatened or actual,
against a person, or against a group or community, that either results
in or has a high likelihood of resulting in injury, death,
psychological harm, maldevelopment or deprivation. This definition
associates intentionality with the committing of the act itself,
irrespective of the outcome it produces.

--- fill

### Participate ###

Need clarification? Have a suggestion? Want to change something? 

* [Join the conversation](http://lists.pirateparty.org.in)
* [View/Edit wiki](http://wiki.pirateparty.org.in)

---

### Change Log ###

* Version 1.0 Draft 4 ([Tue Jan 22 09:39:52 PST 2013](http://lists.pirateparty.org.in/pipermail/discuss-pirateparty.org.in/2013-January/003416.html))

1. Clarify eligibility for permanent membership.

* Version 1.0 Draft 3 ([Mon Nov 19 21:48:32 PST 2012](http://lists.pirateparty.org.in/pipermail/discuss-pirateparty.org.in/2012-November/001030.html))

1. Position on violence added. We oppose violence.

2. Added a Change Log. It tracks changes made to constitution.

3. Added slide show navigation info. It was confusing to some users.

4. Added link to pdf version. It is easy to keep a local copy now.

* Version 1.0 Draft 2 ([Sat Jun 23 11:25:09 PDT 2012](http://lists.pirateparty.org.in/pipermail/discuss-pirateparty.org.in/2012-June/000030.html))

1. Numbering: We have divided our constitution to Articles, Sections and
Sub-sections for easy reference.

2. New Members: We have introduced a process for accepting new members to
our party.

3. Right to food and housing: We think it is important in a country like
ours to mention basic needs as well.
