# How to install

- install R language (http://www.r-project.org/)
- install devtools library for R from CRAN (http://cran.r-project.org/)
- fire up R shell by typing R in your shell
- within R shell, follow the descriptions given on http://ramnathv.github.com/slidify/ to satisfy slidify dependencies.
- run `library(slidyfy)` after all dependencies installed
- whew, you have slidify installed and ready to use.

Now checkout pirateparty repo from https://gitorious.org/pirateparty-in (I know you already have it by know)
cd to the cloned repo,

$ `cd cloned_repo/build`

start R shell and from there

> `library(slidify)`
> `slidify('../pirateparty-in.Rmd', options = list(framework = 'deck.js', theme = 'swiss', transition = 'horizontal-slide'))`

It will create html files for you from Rmd file.
you can deploy that along with asset folder on your favourite server.
